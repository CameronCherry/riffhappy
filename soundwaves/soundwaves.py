import database
import os
import winsound
import numpy as np

from scipy.io import wavfile


class Note:
    wavfilename = "note_sample.wav"

    def __init__(self, name: str, duration: float, sample_rate: int = 44100, vol: float = 1.0, fade_speed: float = 0.0):
        """
        :type name: str
        :type duration: float
        :type sample_rate: int
        :type vol: float
        :type fade_speed: float

        :param name: Note name and octave reference, e.g. "C5".
        :param duration: Length of note in seconds.
        :param sample_rate: in Hz.
        :param vol: Value between 0 (silence) and 1 (full volume).
        :param fade_speed: Rate at which the note fades to silence (does not affect duration). Value between 0 and 1.
        """
        self.name = name
        if name is not None:
            self.freq = database.frequencies[self.name]
        else:
            self.freq = 0
        self.vol = vol
        self.duration = duration
        self.sample_rate = sample_rate
        self.fade_speed = fade_speed
        self.esm = np.arange(self.duration * self.sample_rate)
        x = max(int((1 - self.fade_speed) * self.esm.shape[0]), 1)
        y = self.esm.shape[0] - x
        self.fade_scale = np.append(np.full(shape=x, fill_value=1.0), np.linspace(start=1.0, stop=0.0, num=y))
        self.waveform = self.vol * np.multiply(self.fade_scale,
                                               np.sin(2 * np.pi * self.esm * self.freq / self.sample_rate))

    @property
    def wave_array_int(self):
        wave_array_int = np.int16(self.waveform * np.iinfo(np.int16).max)
        wave_array_int[0] = 0
        return wave_array_int

    def play(self):
        try:
            self.write_to_wav()
            winsound.PlaySound(self.wavfilename, winsound.SND_LOOP)
            self.delete_wav()
        except PermissionError:
            pass

    def write_to_wav(self):
        wavfile.write(self.wavfilename, self.sample_rate, self.wave_array_int)

    def delete_wav(self):
        os.remove(self.wavfilename)

    def write_to_csv(self, csv_file_name):
        np.savetxt(csv_file_name, self.wave_array_int, delimiter=',')

    def write_fade_to_csv(self, csv_file_name):
        np.savetxt(csv_file_name, np.int16(self.fade_scale * np.iinfo(np.int16).max), delimiter=',')


class Chord:
    BLANK_CHORD = [None]

    def __init__(self, note_names, duration, fade_speed, sample_rate):
        """
        :type note_names: list
        :type duration: float
        :type fade_speed: float

        :param note_names: list of note names, e.g. ["C#5", "F#5"].
        :param duration: length of chord in seconds.
        :param fade_speed: speed at which chord fades to silence at the end. Value between 0 (no fade) and 1 (immediate
        fade).
        """
        self.note_names = note_names
        self.duration = duration
        self.fade_speed = fade_speed
        self.sample_rate = sample_rate
        self.notes = [Note(name=note_name, duration=duration, fade_speed=fade_speed, sample_rate=sample_rate)
                      for note_name in note_names]
        self.number_of_notes = len(note_names)

    @property
    def wave_array(self):
        return np.dstack([note.waveform for note in self.notes])


class Riff:
    wavfilename = "sample.wav"

    def __init__(self, chords_list, tempo, sample_rate: int = 44100, intra_fade_rate: float = 0.1):
        """
        Object built from a chords list. Combine sequential chords to create a playable object.

        :type chords_list: list
        :type tempo: int
        :type sample_rate: int
        :type intra_fade_rate: float

        :param chords_list: list of either:
         -- Chord objects; or
         -- lists, where each of the inner list's elements are the note names for the corresponding chord (insert a
            blank list element for silence).
        :param tempo: in bpm
        :param sample_rate: in Hz
        :param intra_fade_rate: decimal between 0 and 1 to determine how quickly each Chord fades to silence.
        """
        self.tempo = tempo
        self.duration = 60 / (self.tempo * 2)
        self.chords = [Chord(chord_notes, fade_speed=intra_fade_rate, duration=self.duration, sample_rate=sample_rate)
                       if type(chord_notes) != Chord else chord_notes for chord_notes in chords_list]
        self.max_notes_in_chord = max([chord.number_of_notes for chord in self.chords])
        self.sample_rate = sample_rate

    @property
    def wave_array_int(self):
        wave_array = np.empty(shape=1)
        for chd in self.chords:
            wave_array = np.append(wave_array, chd.wave_array)
        wave_array_int = np.int16(wave_array * np.iinfo(np.int16).max)
        wave_array_int[0] = 0
        return wave_array_int

    def play(self):
        try:
            self.write_to_wav()
            winsound.PlaySound(self.wavfilename, winsound.SND_LOOP)
        except PermissionError:
            pass

    def write_to_wav(self):
        wavfile.write(self.wavfilename, self.sample_rate, self.wave_array_int)

    def delete_wav(self):
        os.remove(self.wavfilename)

    def write_to_csv(self, csv_file_name):
        np.savetxt(csv_file_name, self.wave_array_int, delimiter=',')


class Riffs:
    def __init__(self, riffs):
        """
        Container for multiple riffs. Enables simultaneous playback of multiple riffs.

        Note that the total playback time will be the length of the shortest riff.

        :type riffs: tuple
        :param riffs: tuple of Riff objects.


        Example:
        ========
        >>> chords_list1 = [["F5"], ["G5"], ["A6"]]
        >>> chords_list2 = [["C5", "E5"], ["C5"]]
        >>> r1 = Riff(chords_list=chords_list1, tempo=120)
        >>> r2 = Riff(chords_list=chords_list1, tempo=120)
        >>> rs = Riffs((r1, r2))
        >>> rs.simultaneous_play()
        """
        self.riffs = riffs
        self.wavfilenames = [f"simultaneous_sample{i}.wav" for i in range(len(riffs))]

    def write_to_wav(self):
        for i in range(len(self.riffs)):
            wavfile.write(self.wavfilenames[i], self.riffs[i].sample_rate, self.riffs[i].wave_array_int)

    def delete_wav(self):
        for i in range(len(self.riffs)):
            os.remove(self.wavfilenames[i])

    def simultaneous_play(self):
        try:
            self.write_to_wav()
            winsound.PlaySound(self.wavfilenames[0], winsound.SND_FILENAME | winsound.SND_ASYNC)
            for i in range(len(self.riffs) - 1):
                winsound.PlaySound(self.wavfilenames[i + 1], winsound.SND_FILENAME)
            self.delete_wav()
        except PermissionError:
            pass


# n = Note(name="C3", duration=1.0, fade_speed=0.8)
# n.play()
# n.write_fade_to_csv("fade2.csv")
# chords_list1 = [["F5"], ["G5"], ["A6"]]
# chords_list2 = [["C5", "E5"], ["C5"]]
# chords_list3 = [["C5", "E5", "G5"]]
# r1 = Riff(chords_list=chords_list1, tempo=120)
# r1.write_to_csv("monad.csv")
# r1.play()
# r2 = Riff(chords_list=chords_list2, tempo=120)
# r2.write_to_csv("dyad.csv")
# r2.play()
# r3 = Riff(chords_list=chords_list3, tempo=120)
# r3.write_to_csv("triad.csv")
# r3.play()

# rs = Riffs((r1, r2))
# rs.simultaneous_play()
