import os

from gui.gui import display_main_gui, update_gui_for_playing_state
from gui.gui import update_button_colour, shift_grid, clear_grid, display_settings_gui, close_settings_gui,\
    apply_new_settings, switch_window_focus, display_popup
from default_settings import default_settings

from file_load import save_grid, load_grid

from gui.grid import Grid


if __name__ == "__main__":

    settings = default_settings
    main_window = display_main_gui(settings)

    grid = Grid(window=main_window, size=default_settings["grid_size"])

    while True:
        event, values = main_window.read()
        if event in (None, 'Exit'):  # i.e. 'X' button or File>Exit
            break
        elif type(event) == tuple:  # if button in grid pressed
            update_button_colour(main_window, key=event, to_initial=False, settings_dict=settings)
        elif event in ('ShiftGridUp', 'ShiftGridDown'):  # shifting grid by 1 semitone
            grid.shift(up=(event == 'ShiftGridUp'))
            shift_grid(main_window, shift_up=(event == 'ShiftGridUp'), grid_size=settings['grid_size'])
        elif event in ('ClearGrid', 'Clear          Ctrl+W'):  # deselects all highlighted grid notes
            grid.clear()
            clear_grid(main_window, settings_dict=settings)
        elif event == 'Settings     Ctrl+T':
            settings_window = display_settings_gui(settings_dict=settings)
            switch_window_focus(from_window=main_window, to_window=settings_window)
            while True:
                s_event, s_values = settings_window.read()
                continue_close = False
                if s_event in (None, 'CancelSettings'):
                    continue_close, new_settings = close_settings_gui(apply_changes=False, settings_dict_curr=settings)
                elif s_event == 'ApplySettings':
                    continue_close, new_settings = close_settings_gui(apply_changes=True, settings_dict_curr=settings,
                                                                      window_values=s_values)
                    settings = new_settings
                    main_window = apply_new_settings(window=main_window, new_settings=settings)
                    grid = Grid(window=main_window, size=settings["grid_size"])
                if continue_close:  # TODO: when apply settings changes, load grid's previous state
                    switch_window_focus(from_window=settings_window, to_window=main_window, close_from_window=True)
                    break
        elif event == 'Play':  # Playback of grid
            update_gui_for_playing_state(main_window, playing=True)
            grid.play(tempo=settings["tempo"], sample_rate=settings["sample_rate"])
            update_gui_for_playing_state(main_window, playing=False)
        elif event == 'About...':
            os.system("start \"\" https://gitlab.com/CameronCherry/riffhappy/")
        elif event == 'Save':
            if grid.is_empty():
                display_popup(save_grid=True)
            else:
                filename = display_popup(filename_for_save_grid=True)
                if filename is not None:
                    save_grid(window=main_window, win_settings=settings, file_name=filename)
        elif event in ('FileToLoad', 'Load'):
            filename = values["FileToLoad"]
            main_window, settings = load_grid(window=main_window, file_name=filename)
            grid = Grid(window=main_window, size=settings["grid_size"])
        else:
            print(f"{event}, {values}")
    main_window.Close()  # Not forgetting to close window at end
