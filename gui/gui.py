import PySimpleGUI as sg
from database import frequencies


# Initialise layout for settings window. PySimpleGUI does not allow layouts to be reused so function is required - for
# example if the user closes then reopens the settings window.
def init_settings_gui_layout(settings) -> list:
    grid_size = settings['grid_size']
    tempo = settings['tempo']
    settings_layout = [[sg.Spin(values=[i for i in range(1, 14)], initial_value=grid_size[0], pad=(10, 10),
                                key='setRows'),
                        sg.Text('Rows', pad=(10, 10))],
                       [sg.Spin(values=[i for i in range(1, 14)], initial_value=grid_size[1], pad=(10, 10),
                                key='setCols'),
                        sg.Text('Columns', pad=(10, 10))],
                       [sg.Spin(values=[i for i in range(40, 141)], initial_value=tempo, pad=(10, 10),
                                key='setTempo'),
                        sg.Text('Tempo (bpm)', pad=(10, 10))],
                       [sg.Button('Apply', key='ApplySettings'), sg.Button('Cancel', key='CancelSettings')]]
    return settings_layout


# Window menu bar
def init_main_layout_header():
    menu_def = [['&File', ['&Open     Ctrl+O', '&Save       Ctrl+S', 'E&xit']],
                ['&Edit', ['Se&ttings     Ctrl+T', '&Clear          Ctrl+W']],
                ['&Help', ['&Quick start', '&About...']]]
    header = [[sg.Menu(menu_def, tearoff=False, pad=(200, 1))]]
    return header


# Top of window but under the menu bar (if required)
def init_main_layout_body_top():
    pass


# Grid of buttons, as well as labels and arrow buttons for shifting grid
def init_main_layout_grid(grid_size: tuple, top_note: str):
    layout = [[sg.Button(sg.SYMBOL_UP, size=(5, 1), key="ShiftGridUp")]]
    # layout = [[sg.Button("UP", size=(5, 1), key="ShiftGridUp")]]
    # matrix of buttons in list form
    # metadata=False means that button is inactive, whilst metadata=True means that button is active
    grid = [[sg.Button(' ', size=(6, 3), key=(i, j), button_color=('#521477', '#521477'), metadata=False)
             for j in range(grid_size[1])] for i in range(grid_size[0])]
    note_names = list(frequencies.keys())
    for btn_row in grid:
        i = grid.index(btn_row)
        note = note_names[i + note_names.index(top_note)]
        btn_row.insert(0, sg.Text(f"\n{note}", key=f"Row{i}NoteLabel", size=(6, 3), justification='center',
                                  metadata=note))
        layout.append(btn_row)
    layout.append([sg.Button(sg.SYMBOL_DOWN, size=(5, 1), key="ShiftGridDown")])
    # layout.append([sg.Button("DOWN", size=(5, 1), key="ShiftGridDown")])
    return layout


# Footer buttons for layout
def init_main_layout_footer():
    footer_spacer = [sg.Text('', pad=(1, 6))]
    # footer_playback = [sg.Button(f"Play {sg.SYMBOL_RIGHT}", key='Play', pad=(8, 8), tooltip='Play the current grid')]
    footer_playback = [sg.Button(f"Play {sg.SYMBOL_RIGHT}", key='Play', pad=(8, 8), tooltip='Play the current grid')]
    footer_shortcuts = [
        sg.Button('Clear grid', key='ClearGrid', pad=(8, 8), tooltip='Clear and reset the current grid'),
        sg.Text(' ' * 50),
        sg.Input(key="FileToLoad", visible=False, enable_events=True),
        sg.FileBrowse('Load riff', key='Load', pad=(8, 8), file_types=[('JSON', '*.json')]),
        sg.Button('Save riff', key='Save', pad=(8, 8)),
    ]
    footer = [footer_spacer, footer_playback, footer_shortcuts]
    return footer


# Updates button colour when user clicks on it.  Also updates metadata attached to button to signal if button is active
# (used for playback)
def update_button_colour(window, key: str, to_initial: bool, settings_dict: dict, from_file: bool = False):
    btn = window[key]
    inactive_colour = settings_dict['theme']['grid_colour']
    active_colour = settings_dict['theme']['grid_highlight_colour']
    if from_file:
        is_active = not settings_dict["grid_state"][key[0]][key[1]]
    else:
        is_active = btn.metadata or to_initial
    new_color = (inactive_colour, inactive_colour) if is_active else \
        (active_colour, active_colour)
    btn.Update(button_color=new_color)
    btn.metadata = not is_active


# Moves grid up or down, subject to bound of frequencies database. Called by clicking on up or down arrows in gui.
def shift_grid(window, grid_size: tuple, shift_up: bool):
    top_note = window[f"Row{0}NoteLabel"].metadata
    note_list = list(frequencies.keys())
    index_of_new_top_note = note_list.index(top_note) - 2 * shift_up + 1
    if (index_of_new_top_note + grid_size[0] > len(note_list) and not shift_up) \
            or (index_of_new_top_note < 0 and shift_up):
        pass
    else:
        for i in range(grid_size[0]):
            new_val = note_list[index_of_new_top_note + i]
            label_elem = window[f"Row{i}NoteLabel"]
            label_elem.metadata = new_val
            label_elem.Update(value=f"\n{new_val}")


# Checks if grid is empty. Used for clearing grid and avoiding unnecessary playback on empty grid.
def grid_is_empty(window, grid_size: tuple) -> bool:
    is_empty = True  # innocent until proven guilty
    for i in range(grid_size[0]):
        for j in range(grid_size[1]):
            o_key = (i, j)
            if window[o_key].metadata:
                is_empty = False
                break
    return is_empty


# Clears grid of all highlighted buttons and resets metadata (i.e. active status) to False
def clear_grid(window, settings_dict: dict):
    grid_size = settings_dict['grid_size']
    if grid_is_empty(window, grid_size):
        display_popup(clear_grid=True)
    else:
        popup = sg.PopupYesNo('This will clear the current grid and any unsaved riffs will be lost.\n\nAre you sure?',
                              title='Clear grid - Warning!')
        if popup == 'Yes':
            for i in range(grid_size[0]):
                for j in range(grid_size[1]):
                    o_key = (i, j)
                    window[o_key].metadata = False
                    update_button_colour(window, key=o_key, to_initial=True, settings_dict=settings_dict)


# Called at startup
def display_main_gui(settings_dict):
    sg.theme(settings_dict['theme']["theme_name"])
    sg.set_options(element_padding=(0, 0))
    # Note that the main window settings is a dictionary which this function creates a layout from. Layouts cannot be
    # reused by PySimpleGUI, so a 'new' layout is required each time. Therefore a function is required to create these
    # 'new' layouts.
    win_layout = []
    header = init_main_layout_header()
    # body_top = init_main_layout_body_top()
    grid = init_main_layout_grid(grid_size=settings_dict['grid_size'], top_note=settings_dict['top_note'])
    footer = init_main_layout_footer()
    for section in [header, grid, footer]:
        for elem in section:
            win_layout.append(elem)
    window = sg.Window(title='RiffHapPy', layout=win_layout, return_keyboard_events=True)
    return window


# Called when user opens settings window
def display_settings_gui(settings_dict: dict):
    settings_gui_layout = init_settings_gui_layout(settings=settings_dict)
    window = sg.Window(title='Settings', layout=settings_gui_layout, disable_minimize=True, finalize=True)
    return window


def close_settings_gui(apply_changes: bool, settings_dict_curr: dict, window_values=None,
                       show_popup: bool = True):
    continue_close = False
    settings_dict_new = settings_dict_curr
    if not apply_changes:
        if show_popup:
            popup = sg.popup_yes_no("Changes will not be applied. Are you sure you want to cancel?", title="Attention!",
                                    keep_on_top=True)
            if popup == "Yes":
                continue_close = True
    else:
        settings_dict_new['grid_size'] = (window_values['setRows'], window_values['setCols'])
        settings_dict_new['tempo'] = window_values['setTempo']
        continue_close = True
    return continue_close, settings_dict_new


# Returns list of *columns* of the grid.  Each column is a list and each element is a list of
# [note_name: str, active: bool]
def get_note_names(window, grid_size: tuple) -> list:
    note_labels = get_displayed_note_labels(window, row_count=grid_size[0])
    grid = [[[note_labels[j], window[(i, j)].metadata] for j in range(grid_size[1])] for i in range(grid_size[0])]
    return grid


# Gets list of string values of the notes that the grid's rows correspond to. Looks in metadata of labels for note name.
def get_displayed_note_labels(window, row_count: int) -> list:
    note_labels = []
    for i in range(row_count):
        note_labels.append(window[f"Row{i}NoteLabel"].metadata)
    return note_labels


def update_gui_for_playing_state(window, playing: bool):  # TODO: Enable support for gui update on playback
    play_btn = window['Play']
    if playing:
        play_btn.Update(text='Now playing...')
    else:
        play_btn.Update(text=f"Play {sg.SYMBOL_RIGHT}")


def request_filename_for_save_grid():
    # f_name = sg.popup_get_file('Enter a filename', save_as=False, default_extension='.json',
    #                            file_types=(('JSON', '.json'),))
    f_name = sg.popup_get_text('Enter a filename (e.g. \"Riff_1.json\"):')
    return f_name


# Apply new settings to main window, such as resizing of grid.
def apply_new_settings(window, new_settings):
    window.Close()
    window = display_main_gui(new_settings)
    window.Finalize()
    return window


def switch_window_focus(from_window, to_window, close_from_window: bool = False):
    from_window.Disable()
    if close_from_window:
        from_window.Close()
    to_window.Finalize()
    to_window.BringToFront()


def display_popup(clear_grid: bool = False, save_grid: bool = False, filename_for_save_grid: bool = False):
    if clear_grid:
        sg.Popup('Grid is already empty!', title='Clear grid')
        return None
    elif save_grid:
        sg.Popup('Grid is empty! Nothing to save...', title='Not saved')
        return None
    elif filename_for_save_grid:
        return request_filename_for_save_grid()
