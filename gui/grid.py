from default_settings import default_settings
from database import frequencies

from soundwaves.soundwaves import Riff, Chord


class Grid:

    inactive_colour = default_settings['theme']['grid_colour']
    active_colour = default_settings['theme']['grid_highlight_colour']
    all_notes = list(frequencies.keys())

    def __init__(self, window, size):
        """
        :type window: PySimpleGUI.PySimpleGUI.Window
        :param window: Window which contains the GUI representation of the grid.
        :param size: Grid size (rows, columns).
        """
        self.window = window
        self.size = size
        self.rows, self.columns = size
        self.top_note = window[f"Row{0}NoteLabel"].metadata
        self._top_note_index = Grid.all_notes.index(self.top_note)
        self.note_list = Grid.all_notes[self._top_note_index: self._top_note_index + self.rows + 1]
        self.state = [[None]] * self.columns
        self.chords_list = None

    def resize(self, new_size):
        self.size = new_size
        self.rows, self.columns = new_size
        self.note_list = Grid.all_notes[self._top_note_index: self._top_note_index + self.rows + 1]
        self.state = [[None]] * self.columns

    def clear(self):
        self.state = [[None]] * self.columns

    def is_empty(self):
        return not any([any([note[1] for note in note_col]) for note_col in self.state])

    @property
    def state(self):
        return self.state

    @state.setter
    def state(self, value):
        self._state = value

    @state.getter
    def state(self):
        return [[[self.note_list[i], self.window[(i, j)].metadata] for i in range(self.rows)]
                for j in range(self.columns)]

    def shift(self, up: bool):
        self._top_note_index = Grid.all_notes.index(self.top_note) - 2 * up + 1
        self.top_note = Grid.all_notes[self._top_note_index]
        self.note_list = Grid.all_notes[self._top_note_index: self._top_note_index + self.rows + 1]

    def play(self, tempo, sample_rate):
        if self.is_empty():
            pass
        else:
            chords_list = []
            for note_column in self.state:
                # if no active notes
                if not any([note[1] for note in note_column]):
                    chords_list.append(Chord.BLANK_CHORD)
                else:
                    chords_list.append([note[0] for note in note_column if note[1]])
            r = Riff(chords_list=chords_list, tempo=tempo, sample_rate=sample_rate)
            r.play()
