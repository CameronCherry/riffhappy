# RiffHappy

A GUI riff generator written in Python.

## Setup

To run RiffHappy, run the following commands.

```console
git clone https://gitlab.com/CameronCherry/riffhappy.git
cd riffhappy
pip3 install -r requirements.txt
```

Note that RiffHappy requires Python 3 to be installed on your system.

## Running

Run the following commands to start RiffHappy.

```console
cd riffhappy
python3 main.py
```

## Screenshots

### Main screen

![Main screen](https://gitlab.com/CameronCherry/riffhappy/-/blob/0c7db366501010f86081cdd900474861fcbd5bf0/screenshots/Main_Window.PNG)

### Example riff

![Example riff](https://gitlab.com/CameronCherry/riffhappy/-/blob/0c7db366501010f86081cdd900474861fcbd5bf0/screenshots/Main_Window_Sample_Riff.PNG)

### Settings menu

Accessed through `Edit > Settings`

![Settings menu](https://gitlab.com/CameronCherry/riffhappy/-/blob/0c7db366501010f86081cdd900474861fcbd5bf0/screenshots/Settings_Window.PNG)

## License

Distributed under the 
[GNU GPL v3.0](https://gitlab.com/CameronCherry/riffhappy/-/blob/0c7db366501010f86081cdd900474861fcbd5bf0/LICENSE).
