import json
import re
from gui.gui import update_button_colour, apply_new_settings


def save_grid(window, win_settings: dict, file_name: str):
    grid_size = win_settings['grid_size']
    grid_active_states = []
    for i in range(grid_size[0]):
        col_active_states = []
        for j in range(grid_size[1]):
            btn_key = (i, j)
            btn = window[btn_key]
            col_active_states.append(btn.metadata)
        grid_active_states.append(col_active_states)
    win_settings["grid_state"] = grid_active_states
    file_name = file_name_valid(file_name)
    if file_name is not None:
        try:
            with open(file_name, 'w', encoding='utf-8') as f:
                json.dump(win_settings, f, ensure_ascii=False, indent=4)
        except FileExistsError:
            pass
    else:
        pass


def file_name_valid(file_name: str):
    if not re.search(r"\w+\.json$", file_name):
        file_name = f"{file_name}.json"
    if re.search(r"\w+\.json", file_name):
        return file_name
    else:
        return None


def load_grid(window, file_name):
    with open(file_name, 'r') as f:
        new_settings = json.load(f)
    grid_size = new_settings['grid_size']
    window = apply_new_settings(window, new_settings)
    for i in range(grid_size[0]):
        for j in range(grid_size[1]):
            btn_key = (i, j)
            update_button_colour(window, key=btn_key, to_initial=False, settings_dict=new_settings, from_file=True)
    return window, new_settings
