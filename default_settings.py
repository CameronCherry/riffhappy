default_settings = {'theme': {'theme_name': 'DarkPurple6',
                              'grid_colour': '#521477',
                              'grid_highlight_colour': '#c327ab'},  # TODO: enable support for alternate themes in
                                                                    # settings
                    'grid_size': (8, 8),  # (rows,columns) Can be changed in settings
                    'top_note': "G5",  # Grid can be shifted in settings
                    'tempo': 120,  # bpm
                    'sample_rate': 44100,  # in Hz
                    }
